# Python templates

## Pylint

*Pylint is a tool used to check code quality according to the set of best practices described in PEP8. This pylint job template will help you to quickly get a working pipeline to validate python code quality using pylint.*

### Quickstart

```yaml
- project: 'devops/gitlab-ci-templates'
  ref: v1
  file:
    - '/templates/python/pylint/pylint.yaml'
```

### Configuration

|environment variable|description|Optional/required|
|-|-|-|
|PYLINT_RCFILE|Path to `.pylintrc` file in the project.|Optional|

## Pytest

*Pytest is a command line tool used in unit testing for python projects. This pytest job template will help you to quickly get a working pipeline to ensure non-regression by running unittest with pytest.*

### Quickstart

```yaml
- project: 'devops/gitlab-ci-templates'
  ref: v1
  file:
    - '/templates/python/pylint/pytest.yaml'
```

### Configuration

|environment variable|description|Optional/required|
|-|-|-|
||||
