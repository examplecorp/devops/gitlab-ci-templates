# Introduction

Here you can find information on how to configure GitLab CI pipelines using generic corporate templates.

## Usage

CORP Gitlab CI templates are stored in repository [devops/gitlab-ci-templates](https://gitlab.com/examplecorp/devops/gitlab-ci-templates).
You can use these templates by including them in your project's `.gitlab-ci.yml`.

```yaml
- project: 'devops/gitlab-ci-templates'
  ref: v1
  file:
    - '/templates/python/pytest.yaml'
    - '/templates/python/pylint.yaml'
```

Most often, you can configure included templates using environment variables. 

```yaml
variables:
  MY_ENV_VAR: value
```

Please have a look at template specific documentation to learn more about possible configuration.
